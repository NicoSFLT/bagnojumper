<!DOCTYPE html>
<html lang="it">
<?php
  session_start();
  $servername = "localhost";
  $db = "progetto";
  $username = "root";
  $password = "";
  $numero = $_SESSION['bagno'];
  $nome = $_SESSION['nome'];

  try {
   $conn = new PDO("mysql:host=$servername;dbname=$db", $username,$password);
   //se qualcosa va storto, si cattura l’eccezione, altrimenti..
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   }
  catch(PDOException $e)
   {
   echo "Connection failed: " . $e->getMessage();
   }

   $sql="SELECT * FROM cabine WHERE IDBagno=$numero";
   $query = $conn->query($sql);
   $ncab = $query->rowCount();

   $sql="SELECT * FROM cabine WHERE IDBagno=$numero ORDER BY IDCabina";
   foreach($conn->query($sql) as $row){
     $num[]=$row['IDCabina'];
     $disp[]=$row['Occupata'];
   }
 ?>
<style media="screen">
table  {
    border-collapse:collapse
}
td, th {
    border:1px solid #ddd;
    padding:8px;
}
.tab{
  width: 40%;
}
</style>
  <head>
    <meta charset="utf-8">
    <title>Bagno Jumper - Cabine</title> <!-- Pagina generale per le Cabine, sarà la stessa per tutti, ma con php a seconda del bagno di origine cambiano i dati dentro -->
  </head>
  <body>
    <div>
      <h1 style="color: red"><center>Cabine - <?php echo "Bagno $nome" ?></center></h1>
      <center>
        <table class="tab">
          <tr><td style="font-weight: bolder">Cabine</td><td style="font-weight: bolder">Stato</td><td style="font-weight: bolder">Prenota</td></tr> <!-- A seconda della disponibilità del database il bottone sarà abilitato o meno -->
          <?php for($i=0;$i<$ncab;$i++){
            echo "<tr><td>" ;
            echo $num[$i] ;
            echo "</td><td>";
            if ($disp[$i]==0){
              echo $disp[$i] ;
              echo "</td><td>";
              echo "<center><a href=\"busycab.php?cabina=$num[$i]\">Prenota</a></center></td></tr>";
            }else{
              echo $disp[$i] ;
              echo "</td><td>";
              echo "<center><a href=\"freecab.php?cabina=$num[$i]\">Libera</a></center></td></tr>";
            }
          } ?>
        </table>
      </ceter>
    </div>
    <div style="padding: 20px">
      <center> <button  type="button" name="home" onclick="location.href='bagno.php';">Torna ai Servizi</button></center>
    </div>
    <div >
      <center> <button  type="button" name="home" onclick="location.href='home.php';">Home</button></center>
    </div>
  </body>
</html>
