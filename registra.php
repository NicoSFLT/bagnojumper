<!DOCTYPE html>
<html lang="it">
<?php
  session_start();
  $servername = "localhost";
  $db = "progetto";
  $username = "root";
  $password = "";
  $nomee = $_SESSION['nome'];

  try {
   $conn = new PDO("mysql:host=$servername;dbname=$db", $username,$password);
   //se qualcosa va storto, si cattura l’eccezione, altrimenti..
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   }
  catch(PDOException $e)
   {
   echo "Connection failed: " . $e->getMessage();
   }

   $sql="SELECT * FROM clienti";
   $query = $conn->query($sql);
   $ncli = $query->rowCount();

 ?>
<style media="screen">
table  {
    border-collapse:collapse
}
td, th {
    border:1px solid #ddd;
    padding:8px;
}
.tab{
  width: 60%;
}
</style>
  <head>
    <meta charset="utf-8">
    <title>Bagno Jumper - Registrazione Cliente</title> <!-- Pagina generale per le Cabine, sarà la stessa per tutti, ma con php a seconda del bagno di origine cambiano i dati dentro -->
  </head>
  <body>
    <div>
      <h1 style="color: red"><center>Registra Cliente <?php echo "Bagno $nomee - " ?> (ID Ultimo cliente registrato: <?php echo $ncli ?>)</center></h1>
      <center><form action="tmp.php" method="post">
        ID: <input type="number" name="idcliente"><br>
        Name: <input type="text" name="nome"><br>
        Cognome: <input type="text" name="cognome"><br>
        Codice Fiscale: <input type="text" name="cf"><br>
        Indirizzo: <input type="text" name="indirizzo"><br>
        <input type="submit">
      </form></center>
    </div>
    <div style="padding: 20px">
      <center> <button  type="button" name="home" onclick="location.href='bagno.php';">Torna ai Servizi</button></center>
    </div>
    <div >
      <center> <button  type="button" name="home" onclick="location.href='home.php';">Home</button></center>
    </div>
  </body>
</html>
