<!DOCTYPE html>
<html lang="it">
<?php
  session_start();
  $servername = "localhost";
  $db = "progetto";
  $username = "root";
  $password = "";
  $nomee = $_SESSION['nome'];
  $numero = $_SESSION['bagno'];

  try {
   $conn = new PDO("mysql:host=$servername;dbname=$db", $username,$password);
   //se qualcosa va storto, si cattura l’eccezione, altrimenti..
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   }
  catch(PDOException $e)
   {
   echo "Connection failed: " . $e->getMessage();
   }

   $sql="SELECT * FROM ombrelloni WHERE Occupato=0 AND IDBagno=$numero";
   $query = $conn->query($sql);
   $nomb = $query->rowCount();
   foreach($conn->query($sql) as $row){
     $omb[]=$row['IDOmbrellone'];
   }

   $sql="SELECT * FROM cabine WHERE Occupata=0 AND IDBagno=$numero";
   $query = $conn->query($sql);
   $ncab = $query->rowCount();
   foreach($conn->query($sql) as $row){
     $cab[]=$row['IDCabina'];
   }

   $sql="SELECT * FROM clienti";
   $query = $conn->query($sql);
   $ncli = $query->rowCount();
   foreach($conn->query($sql) as $row){
     $cli[]=$row['IDCliente'];
   }

 ?>
<style media="screen">
table  {
    border-collapse:collapse
}
td, th {
    border:1px solid #ddd;
    padding:8px;
}
.tab{
  width: 60%;
}
</style>
  <head>
    <meta charset="utf-8">
    <title>Bagno Jumper - Registrazione Prenotazione</title> <!-- Pagina generale per le Cabine, sarà la stessa per tutti, ma con php a seconda del bagno di origine cambiano i dati dentro -->
  </head>
  <body>
    <div>
      <h1 style="color: red"><center>Registra Prenotazione - <?php echo "Bagno $nomee " ?></center></h1>
      <center><form action="pre.php" method="post">
        Cliente: <select name="cliente">
          <?php for($i=0; $i<$ncli; $i++){
              echo "<option value=\"$cli[$i]\">$cli[$i]</option>";
          } ?>
        </select><br>
        Inizio: <input type="date" name="inizio"><br>
        Fine: <input type="date" name="fine"><br>
        Ombrellone: <select name="ombrelloni">
          <?php for($j; $j<$nomb; $j++){
              echo "<option value=\"$omb[$j]\">$omb[$j]</option>";
          } ?></select><br>
          Cabina: <select name="cabine">
            <?php for($k=0; $k<$ncab; $k++){
                echo "<option value=\"$cab[$k]\">$cab[$k]</option>";
            } ?></select><br>
        Pacchetto: <input type="number" name="pacchetto"><br>
        <input type="submit">
      </form></center>
    </div>
    <div style="padding: 20px">
      <center> <button  type="button" name="home" onclick="location.href='bagno.php';">Torna ai Servizi</button></center>
    </div>
    <div >
      <center> <button  type="button" name="home" onclick="location.href='home.php';">Home</button></center>
    </div>
  </body>
</html>
