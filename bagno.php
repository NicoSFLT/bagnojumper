<!DOCTYPE html>
<html lang="it">
<?php
  session_start();
  $servername = "localhost";
  $db = "progetto";
  $username = "root";
  $password = "";
  $numero = $_SESSION['bagno'];
  $nome = $_SESSION['nome'];
  try {
   $conn = new PDO("mysql:host=$servername;dbname=$db", $username,$password);
   //se qualcosa va storto, si cattura l’eccezione, altrimenti..
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   }
  catch(PDOException $e)
   {
   echo "Connection failed: " . $e->getMessage();
   }

 ?>
<style>
table  {
    border-collapse:collapse
}
td, th {
    border:1px solid #ddd;
    padding:8px;
}
.tab1{
  width: 40%;
}
</style>
  <head>
    <title>Bagno Jumper - Servizi</title> <!-- Titolo in alto,nelle cartelle, così uno sa in quale bagno sta prenotando -->
  </head>
  <body>
    <div>
      <h1 style="color: red"><center>Bagno Jumper - <?php echo $nome ?></center></h1> <!-- Intestazione del bagno in cui siamo -->
      <center>
        <table class="tab1">  <!-- Tabella in cui si vedono i servizi disponibili, dal quale accedo alla pagina specifica, per poter prenotare quello che mi serve -->
          <tr><td style="font-weight: bolder">Nome</td><td style="font-weight: bolder">Link Servizio</td></tr>
          <tr><td>Ombrelloni</td><td><center><button type="button" value="Ombrelloni" onclick="location.href='ombrelloni.php';">Ombrelloni</button></center></td></tr>
          <tr><td>Cabine</td><td><center><button type="button" value="Cabine" onclick="location.href='cabine.php';">Cabine</button></center></td></tr>
          <tr><td>Soggiorni</td><td><center><button type="button" value="Soggiorni" onclick="location.href='soggiorni.php';">Soggiorni</button></center></td></tr>
          <tr><td>Clienti</td><td><center><button type="button" value="Clienti" onclick="location.href='clienti.php';">Clienti</button></center></td></tr>
          <tr><td>Animatori</td><td><center><button type="button" value="Animatori" onclick="location.href='animatori.php';">Animatori</button></center></td></tr>
          <tr><td>Registra Cliente</td><td><center><button type="button" value="Registra" onclick="location.href='registra.php';">Registra</button></center></td></tr>
          <tr><td>Registra Animatore</td><td><center><button type="button" onclick="location.href='registraAnimatore.php';">Registra</button></center></td></tr>
          <tr><td>Prenota</td><td><center><button type="button" value="Prenota" onclick="location.href='prenota.php';">Prenota Soggiorno</button></center></td></tr>
          <tr><td>Ristorante</td><td><center><button type="button" value="ristorante" onclick="location.href='ristorante.php';">Ristorante</button></center></td></tr>
          <tr><td>Prenota Ristorante</td><td><center><button type="button" value="PrenotaRisotrante" onclick="location.href='prenotaRistorante.php';">Prenota Ristorante</button></center></td></tr>
          <tr><td>Statistiche</td><td><center><button type="button" value="Statistiche" onclick="location.href='statistiche.php';">Statistiche</button></center></td></tr>
        </table>
      </ceter>
    </div>
    <div style="padding: 20px">
      <center> <button  type="button" name="home" onclick="location.href='home.php';">Home</button></center>
    </div>
  </body>
</html>
