<!DOCTYPE html>
<html lang="it">
<?php
  session_start();
  $servername = "localhost";
  $db = "progetto";
  $username = "root";
  $password = "";
  $nomee = $_SESSION['nome'];

  try {
   $conn = new PDO("mysql:host=$servername;dbname=$db", $username,$password);
   //se qualcosa va storto, si cattura l’eccezione, altrimenti..
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   }
  catch(PDOException $e)
   {
   echo "Connection failed: " . $e->getMessage();
   }

   $sql="SELECT * FROM clienti";
   $query = $conn->query($sql);
   $ncli = $query->rowCount();

   $sql="SELECT * FROM clienti ORDER BY IDCliente";
   foreach($conn->query($sql) as $row){
     $cli[]=$row['IDCliente'];
     $nome[]=$row['Nome'];
     $cogn[]=$row['Cognome'];
     $cf[]=$row['CF'];
     $ind[]=$row['Indirizzo'];
   }
 ?>
<style media="screen">
table  {
    border-collapse:collapse
}
td, th {
    border:1px solid #ddd;
    padding:8px;
}
.tab{
  width: 60%;
}
</style>
  <head>
    <meta charset="utf-8">
    <title>Bagno Jumper - Clienti</title> <!-- Pagina generale per le Cabine, sarà la stessa per tutti, ma con php a seconda del bagno di origine cambiano i dati dentro -->
  </head>
  <body>
    <div>
      <h1 style="color: red"><center>Clienti - <?php echo "Bagno $nomee - " ?><?php echo $ncli ?></center></h1>
      <center>
        <table class="tab">
          <tr><td style="font-weight: bolder">IDCliente</td><td style="font-weight: bolder">Nome</td><td style="font-weight: bolder">Cognome</td><td style="font-weight: bolder">Cod. Fiscale</td><td style="font-weight: bolder">Indirizzo</td><td style="font-weight: bolder">Elimina</td></tr> <!-- A seconda della disponibilità del database il bottone sarà abilitato o meno -->
          <?php for($i=0;$i<$ncli;$i++){
            echo "<tr><td>" ;
            echo $cli[$i] ;
            echo "</td><td>";
            echo $nome[$i] ;
            echo "</td><td>";
            echo $cogn[$i] ;
            echo "</td><td>";
            echo $cf[$i] ;
            echo "</td><td>";
            echo $ind[$i] ;
            echo "</td><td>";
            echo "<center><a href=\"elimina.php?id=$cli[$i]\">Elimina</a></center></td></tr>";
            }
          ?>
        </table>
      </ceter>
    </div>
    <div style="padding: 20px">
      <center> <button  type="button" name="home" onclick="location.href='bagno.php';">Torna ai Servizi</button></center>
    </div>
    <div >
      <center> <button  type="button" name="home" onclick="location.href='home.php';">Home</button></center>
    </div>
  </body>
</html>
