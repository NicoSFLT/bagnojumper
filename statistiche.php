<!DOCTYPE html>
<html lang="it">
<?php
  session_start();
  $servername = "localhost";
  $db = "progetto";
  $username = "root";
  $password = "";
  $numero = $_SESSION['bagno'];
  $nome = $_SESSION['nome'];
  try {
   $conn = new PDO("mysql:host=$servername;dbname=$db", $username,$password);
   //se qualcosa va storto, si cattura l’eccezione, altrimenti..
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   }
  catch(PDOException $e)
   {
   echo "Connection failed: " . $e->getMessage();
   }

   $sql = "SELECT IDOmbrellone FROM soggiorni WHERE IDBagno=$numero ORDER BY IDOmbrellone";
   $query = $conn->query($sql);
   $n = $query->rowCount();
   foreach($conn->query($sql) as $row){
     $omb[]=$row['IDOmbrellone'];
   }
   $cont=0;
   $maxc=0;
   $maxomb=0;
   $c=0;
   for($i=0;$i<$n;$i++){
     $c=$omb[$i];
     for($j=0;$j<$n;$j++){
       if($omb[$j]==$c){
         $cont++;
       }
     }
     if($cont>$maxc){
       $maxc=$cont;
       $maxomb=$c;
     }
     $cont=0;
   }

   $minc=1000;
   for($i=0;$i<$n;$i++){
     $c=$omb[$i];
     for($j=0;$j<$n;$j++){
       if($omb[$j]==$c){
         $cont++;
       }
     }
     if($minc>$cont){
       $minc=$cont;
       $minomb=$c;
     }
     $cont=0;
   }

   $sql = "SELECT IDBagno FROM ristorante ORDER BY IDBagno";
   $query = $conn->query($sql);
   $np = $query->rowCount();
   foreach($conn->query($sql) as $row){
     $pre[]=$row['IDBagno'];
   }

   $c=0; //scorre i valori del vettore con i bagni
   $cont=0; //$maxc=0; conta le occorrenze di $c
   $maxc=0; // tiene in memoria il numero massimo di occorrenze
   $maxb=0; // tiene in memoria il numero del bagno più prenotato
   for($i=0;$i<$np;$i++){
     $c=$pre[$i];
     for($j=0;$j<$np;$j++){
       if($pre[$j]==$c){
         $cont++;
       }
     }
     if($cont>$maxc){
       $maxc=$cont;
       $maxb=$c;
     }
     $cont=0;
   }
   $stat = 0;
   if($maxb == $numero){
     $stat = 1;
   }

   $sql = "SELECT IDCliente FROM ristorante WHERE IDBagno=$numero ORDER BY IDCliente";
   $query = $conn->query($sql);
   $nc = $query->rowCount();

   foreach($conn->query($sql) as $row){
     $cli[]=$row['IDCliente'];
   }

   $cont=0;
   $maxc=0;
   $maxcli=0;
   $c=0;
   for($i=0;$i<$nc;$i++){
     $c=$cli[$i];
     for($j=0;$j<$nc;$j++){
       if($cli[$j]==$c){
         $cont++;
       }
     }
     if($cont>$maxc){
       $maxc=$cont;
       $maxcli=$c;
     }
     $cont=0;
   }

   $sql = "SELECT DATEDIFF(DataFine,DataInizio),IDOmbrellone,IDCliente FROM soggiorni WHERE IDBagno=$numero ORDER BY IDOmbrellone";
   $query = $conn->query($sql);
   $righe = $query->rowCount();
   foreach($conn->query($sql) as $row){
     $gg[]=$row['DATEDIFF(DataFine,DataInizio)'];
     $ido[]=$row['IDOmbrellone'];
     $idc[]=$row['IDCliente'];
   }
   for($k=0;$k<$righe;$k++){
     $gg[$k]++;
   }
   $maxgg=0;
   for($i=0; $i<$righe; $i++) {
      if( $gg[$i]>$maxgg ) {
        $maxgg=$gg[$i];
        $maxo=$ido[$i];
        $maxcl=$idc[$i];
      }
    }

    /*Statisctiche per gli animatori*/
    $sql = "SELECT Nome, Cognome, Paga FROM animatori WHERE IDBagno=$numero ORDER BY IDAnimatore";
    $query = $conn->query($sql);
    $ra = $query->rowCount();
    foreach($conn->query($sql) as $row){
      $nomea[]=$row['Nome'];
      $cogna[]=$row['Cognome'];
      $pa[]=$row['Paga'];
    }
    $maxs=0;
    for($i=0;$i<$ra;$i++){
      if($pa[$i]>$maxs){
        $maxs=$pa[$i];
        $maxnomea=$nomea[$i];
        $maxcogna=$cogna[$i];
      }
    }

    /*Statistiche relative al bagno*/
    $ciccio = 0;
    $benzina = 0;
    $sql = "SELECT NOmbrelloni, NCabine FROM bagni WHERE IDBagno=$numero";
    $query = $conn->query($sql);
    foreach($conn->query($sql) as $row){
      $nombre=$row['NOmbrelloni'];
      $ncab=$row['NCabine'];
    }
    $sql2= "SELECT MAX(NOmbrelloni), MAX(NCabine) FROM bagni";
    $query = $conn->query($sql2);
    foreach($conn->query($sql2) as $row){
      if($nombre == $row['MAX(NOmbrelloni)']){
        $ciccio = 1;
      }
      if($ncab == $row['MAX(NCabine)']){
        $benzina = 1;
      }
    }
 ?>
<style>
table  {
    border-collapse:collapse
}
td, th {
    border:1px solid #ddd;
    padding:8px;
}
.tab1{
  width: 40%;
}
</style>
  <head>
    <title>Bagno Jumper - Servizi</title> <!-- Titolo in alto,nelle cartelle, così uno sa in quale bagno sta prenotando -->
  </head>
  <body>
    <div>
      <h1><center>Bagno Jumper - <?php echo $nome ?></center></h1> <!-- Intestazione del bagno in cui siamo -->
      <div>
        <h1 style="color: red;"><center>Statistiche relative al Bagno</center></h1>

        <?php if($ciccio == 1){
          echo "<h2><center>Questo Bagno ha il numero massimo di ombrelloni, è il più grande!</center></h2>";
        }else{
          echo "<h2><center>Questo Bagno non è il più grande della compagnia!</center></h2>";
        }
        ?>
        <?php if($benzina == 1){
          echo "<h2><center>Questo Bagno ha il numero massimo di cabine!</center></h2>";
        } else {
          echo "<h2><center>Questo Bagno non ha il numero massimo di cabine!</center></h2>";
        } ?>
        <h2><center><center></h2>
          <h1 style="color: red;"><center>Statistiche relative agli Ombrelloni</center></h1>
        <h2><center>Ombrellone più gradito:  <?php echo $maxomb ?></center></h2>
        <?php if($maxomb != $minomb){
            echo "<h2><center>Ombrellone meno gradito:";
            echo $minomb;
            echo "</center></h2>";
        } ?>
        <h2><center>L'Ombrellone n° <?php echo $maxo ?> è quello con la prenotazione più lunga,</center></h2>
        <h2><center>infatti è stato occupato per ben <?php echo $maxgg ?> giorni dal cliente <?php echo $maxcl ?>.</center></h2>

          <h1 style="color: red;"><center>Statistiche relative al Ristorante</center></h1>
        <?php if($stat == 1) {
          echo "<h2><center>Il tuo Ristorante è il più frequentato della nostra compagnia!</center></h2>";
        } else {
          echo "<h2><center>Le frequenze del tuo Ristorante sono nella media, ma non sei ancora il migliore!</center></h2>";
        } ?>
        <h2><center>Il miglior cliente per questo Ristorante è il n° <?php echo $maxcli ?></center></h2>

        <h1 style="color: red;"><center>Statistiche relative agli Animatori</center></h1>
          <h2><center>L'animatore che ha la paga oraria più alta è <?php echo $maxnomea ?> <?php echo $maxcogna ?></center></h2>            <h2><center>con uno stipendio di <?php echo $maxs ?> € all'ora.<center></h2>
      </div>
      <center>
        <table class="tab1">

        </table>
      </ceter>
    </div>
    <div style="padding: 20px">
      <center> <button  type="button" name="home" onclick="location.href='home.php';">Home</button></center>
    </div>
    <div style="padding: 20px">
      <center> <button  type="button" name="home" onclick="location.href='bagno.php';">Torna ai Servizi</button></center>
    </div>
  </body>
</html>
