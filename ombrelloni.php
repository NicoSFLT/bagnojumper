<!DOCTYPE html>
<html lang="it">
<?php
  session_start();
  $servername = "localhost";
  $db = "progetto";
  $username = "root";
  $password = "";
  $numero = $_SESSION['bagno'];
  $nome = $_SESSION['nome'];

  try {
   $conn = new PDO("mysql:host=$servername;dbname=$db", $username,$password);
   //se qualcosa va storto, si cattura l’eccezione, altrimenti..
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   }
  catch(PDOException $e)
   {
   echo "Connection failed: " . $e->getMessage();
   }

   $sql="SELECT * FROM ombrelloni WHERE IDBagno=$numero";
   $query = $conn->query($sql);
   $nomb = $query->rowCount();

   $sql="SELECT * FROM ombrelloni WHERE IDBagno=$numero ORDER BY IDOmbrellone";
   foreach($conn->query($sql) as $row){
     $num[]=$row['IDOmbrellone'];
     $fila[]=$row['Fila'];
     $disp[]=$row['Occupato'];
   }

 ?>
<style media="screen">
table  {
    border-collapse:collapse
}
td, th {
    border:1px solid #ddd;
    padding:8px;
}
.tab{
  width: 40%;
}
</style>
  <head>
    <meta charset="utf-8">
    <title>Bagno Jumper - Ombrelloni </title> <!-- Pagina generale per gli Ombrelloni, sarà la stessa per tutti, ma con php a seconda del bagno di origine cambiano i dati dentro -->
  </head>
  <body>
    <div>
      <h1 style="color: red"><center>Ombrelloni - <?php echo "Bagno $nome" ?></center></h1>
      <center>
        <table class="tab">
          <tr><td style="font-weight: bolder">Ombrellone</td><td style="font-weight: bolder">Fila</td><td style="font-weight: bolder">Stato</td><td style="font-weight: bolder">Prenota</td></tr> <!-- A seconda della disponibilità del database il bottone sarà abilitato o meno -->
          <?php for($i=0;$i<$nomb;$i++){
            echo "<tr><td>" ;
            echo $num[$i] ;
            echo "</td><td>";
            echo $fila[$i] ;
            echo "</td><td>";
            if ($disp[$i]==0){
              echo $disp[$i] ;
              echo "</td><td>";
              echo "<center><a href=\"busyomb.php?ombrellone=$num[$i]\">Prenota</a></center></td></tr>";
            }else{
              echo $disp[$i] ;
              echo "</td><td>";
              echo "<center><a href=\"freeomb.php?ombrellone=$num[$i]\">Libera</a></center></td></tr>";
            }
          } ?>
          <!--<tr><td><?php echo $numero[0] ?></td><td><?php echo $fila[0] ?></td><td><?php echo $disp[0]?></td><td><center><button type="button">Prenota</button></center></td></tr> -->
        </table>
      </ceter>
    </div>
    <div style="padding: 20px">
      <center> <button  type="button" name="home" onclick="location.href='bagno.php';">Torna ai Servizi</button></center>
    </div>
    <div >
      <center> <button  type="button" name="home" onclick="location.href='home.php';">Home</button></center>
    </div>

  <!--  <div id="saldo">
      <center>
        <button type="button" value="salto" onclick="location.href='saldo.html';">Vai al Saldo</button>
      </center>
    </div> -->
  </body>
</html>
