-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Lug 13, 2017 alle 17:39
-- Versione del server: 10.1.21-MariaDB
-- Versione PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `progetto`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `animatori`
--

CREATE TABLE `animatori` (
  `IDAnimatore` int(11) NOT NULL,
  `Nome` varchar(20) NOT NULL,
  `Cognome` varchar(20) NOT NULL,
  `CF` varchar(30) NOT NULL,
  `datanascita` date NOT NULL,
  `dataassunzione` date NOT NULL,
  `IDBagno` int(11) NOT NULL,
  `Paga` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `animatori`
--

INSERT INTO `animatori` (`IDAnimatore`, `Nome`, `Cognome`, `CF`, `datanascita`, `dataassunzione`, `IDBagno`, `Paga`) VALUES
(2, 'Marco', 'Verdi', 'MRCVRD845CDI8', '1992-08-12', '2017-06-05', 1, 5),
(3, 'Luca', 'Gialli', 'GLLLCU657CFD4', '1995-05-10', '2017-05-20', 2, 9),
(4, 'Claudia', 'Violi', 'VLCLDR895GHC3', '1994-07-22', '2017-05-01', 2, 7),
(5, 'Martina', 'Neri', 'NRMRTN674GBV2', '1985-03-18', '2017-06-01', 3, 6),
(6, 'Ciccio', 'Bastardo', 'CCCBSTRD568RBFR65', '1979-03-09', '2017-06-01', 3, 3),
(7, 'Claudio', 'Ravaglia', 'RVGCLD5478GHJ789', '1994-07-03', '2017-07-01', 1, 7),
(8, 'Asia', 'Bartolini', 'BRTLNS657HGF33', '1998-04-03', '2017-06-01', 1, 6),
(9, 'Luca', 'Porcellini', 'PRCLLLCU45DBF86', '1997-02-03', '2017-06-15', 2, 5),
(10, 'Alberto', 'Savini', 'SVNLBRT34GHU6', '1991-04-04', '2017-08-01', 3, 8);

-- --------------------------------------------------------

--
-- Struttura della tabella `bagni`
--

CREATE TABLE `bagni` (
  `IDBagno` int(10) NOT NULL,
  `Nome` varchar(20) NOT NULL,
  `Numero` int(11) NOT NULL,
  `NOmbrelloni` int(11) NOT NULL,
  `Frazione` varchar(30) NOT NULL,
  `NCabine` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Lista dei Bagni';

--
-- Dump dei dati per la tabella `bagni`
--

INSERT INTO `bagni` (`IDBagno`, `Nome`, `Numero`, `NOmbrelloni`, `Frazione`, `NCabine`) VALUES
(0, 'Nessun Bagno', 0, 0, '0', 0),
(1, 'Bagno Casa', 56, 100, 'Pinarella Di Cervia', 100),
(2, 'Bagno Strada', 88, 150, 'Lido Di Classe', 150),
(3, 'Bagno Gianni', 7, 200, 'Lido Di Savio', 200);

-- --------------------------------------------------------

--
-- Struttura della tabella `cabine`
--

CREATE TABLE `cabine` (
  `IDCabina` int(11) NOT NULL,
  `IDBagno` int(11) NOT NULL,
  `Occupata` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `cabine`
--

INSERT INTO `cabine` (`IDCabina`, `IDBagno`, `Occupata`) VALUES
(0, 0, 0),
(0, 1, 0),
(0, 2, 0),
(0, 3, 0),
(1, 1, 1),
(1, 2, 0),
(1, 3, 0),
(2, 1, 1),
(2, 2, 1),
(2, 3, 0),
(3, 1, 0),
(3, 2, 0),
(3, 3, 0),
(4, 1, 1),
(4, 2, 0),
(4, 3, 1),
(5, 1, 0),
(5, 2, 1),
(5, 3, 1),
(6, 1, 1),
(6, 2, 0),
(6, 3, 0),
(7, 1, 1),
(7, 2, 0),
(7, 3, 0),
(8, 1, 0),
(8, 2, 0),
(8, 3, 0),
(9, 1, 1),
(9, 2, 1),
(9, 3, 1),
(10, 1, 1),
(10, 2, 0),
(10, 3, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `clienti`
--

CREATE TABLE `clienti` (
  `IDCliente` int(11) NOT NULL,
  `Nome` varchar(20) NOT NULL,
  `Cognome` varchar(20) NOT NULL,
  `CF` varchar(20) NOT NULL,
  `Indirizzo` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `clienti`
--

INSERT INTO `clienti` (`IDCliente`, `Nome`, `Cognome`, `CF`, `Indirizzo`) VALUES
(4, 'Riccardo ', 'Soro', 'RCRSRR98I87C765R', 'Via Monda, 90'),
(5, 'Matteo', 'Sacco', 'SCCMTT46895T', 'Via Bologna, 8'),
(6, 'Andrea', 'Casamenti', 'CSMNDR897TDG5', 'Via BluVertigo, 9'),
(7, 'Pippo', 'Baudo', 'BDPPP76HJI9', 'Via Tippete, 7'),
(9, 'Luca', 'Presio', 'PRSLCU76FGR4', 'Via Double, 1'),
(10, 'Mario', 'Bros', 'BRMRO743TF', 'Via Canoetta, 2'),
(11, 'Natascia', 'Bisacchi', 'BSCNTSB763K', 'Via Milfica, 88'),
(12, 'Beppe', 'Pondini', 'PNDBPP443HGA2', 'Via Go Kart, 237'),
(13, 'Bob', 'Kelso', 'KLSBB756HJB9', 'Via Sacro Cuore, 774'),
(14, 'Dorian', 'Grey', 'GRDRN654GH5', 'Via Muffin, 223'),
(16, 'Elisabetta', 'Valentini', 'VLNTLSB654F4', 'Via della Lepre, 4'),
(17, 'Francesca', 'Gatti', 'GTTFRCS5749K9', 'Via Palatina, 4'),
(18, 'Consuelo', 'Poni', 'PNCSL756TG3', 'Via Bulga, 3'),
(19, 'Silvia', 'Panzavolta', 'PNZVLT4648G5', 'Via Firenze, 290'),
(20, 'Emanuele', 'Laghi', 'LGHMNL785G1', 'Via Celletta, 3'),
(23, 'Gianni', 'Barbagianni', 'Ã§OHDOUH', 'Via Milfica, 74'),
(26, 'Ciccio', 'Bastardo', 'VDIUFO', 'Via Milfica, 6969');

-- --------------------------------------------------------

--
-- Struttura della tabella `listinoprezzi`
--

CREATE TABLE `listinoprezzi` (
  `IDPrezzo` int(11) NOT NULL,
  `Servizio` varchar(20) NOT NULL,
  `Fascia` varchar(1) NOT NULL,
  `Stagione` varchar(1) NOT NULL,
  `Prezzo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `listinoprezzi`
--

INSERT INTO `listinoprezzi` (`IDPrezzo`, `Servizio`, `Fascia`, `Stagione`, `Prezzo`) VALUES
(1, 'Ombrellone', 'B', 'B', 8),
(2, 'Ombrellone', 'M', 'B', 12),
(3, 'Ombrellone', 'A', 'B', 15),
(4, 'Ombrellone', 'B', 'M', 11),
(5, 'Ombrellone', 'M', 'M', 15),
(6, 'Ombrellone', 'A', 'M', 18),
(7, 'Ombrellone', 'B', 'A', 14),
(8, 'Ombrellone', 'M', 'A', 18),
(9, 'Ombrellone', 'A', 'A', 21),
(10, 'Cabina', '0', 'B', 3),
(11, 'Cabina', '0', 'M', 4),
(12, 'Cabina', '0', 'A', 5);

-- --------------------------------------------------------

--
-- Struttura della tabella `ombrelloni`
--

CREATE TABLE `ombrelloni` (
  `IDOmbrellone` int(11) NOT NULL,
  `IDBagno` int(11) NOT NULL,
  `Fila` varchar(1) NOT NULL,
  `Occupato` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ombrelloni`
--

INSERT INTO `ombrelloni` (`IDOmbrellone`, `IDBagno`, `Fila`, `Occupato`) VALUES
(0, 0, '0', 0),
(1, 1, 'B', 0),
(1, 2, 'B', 0),
(1, 3, 'B', 0),
(2, 1, 'B', 0),
(2, 2, 'B', 1),
(2, 3, 'B', 1),
(3, 1, 'B', 1),
(3, 2, 'B', 0),
(3, 3, 'B', 1),
(4, 1, 'M', 0),
(4, 2, 'M', 1),
(4, 3, 'M', 0),
(5, 1, 'M', 1),
(5, 2, 'M', 0),
(5, 3, 'M', 1),
(6, 1, 'M', 0),
(6, 2, 'M', 1),
(6, 3, 'M', 1),
(7, 1, 'A', 1),
(7, 2, 'A', 1),
(7, 3, 'A', 1),
(8, 1, 'A', 0),
(8, 2, 'A', 1),
(8, 3, 'A', 0),
(9, 1, 'A', 0),
(9, 2, 'A', 0),
(9, 3, 'A', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `pacchetti`
--

CREATE TABLE `pacchetti` (
  `IDPacchetto` int(11) NOT NULL,
  `NomePacchetto` varchar(30) NOT NULL,
  `Percentuale Sconto` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `pacchetti`
--

INSERT INTO `pacchetti` (`IDPacchetto`, `NomePacchetto`, `Percentuale Sconto`) VALUES
(0, 'Nessun Pacchetto', '0'),
(1, 'Pacc Bis:  Ombrellone+Cabina', '10%'),
(2, 'Pacchetto 1 Mese', '300'),
(3, 'Pacchetto 2 Mesi', '450'),
(4, 'Pacchetto 3 Mesi', '600');

-- --------------------------------------------------------

--
-- Struttura della tabella `ristorante`
--

CREATE TABLE `ristorante` (
  `IDPrenotazione` int(11) NOT NULL,
  `IDBagno` int(11) NOT NULL,
  `IDCliente` int(11) NOT NULL,
  `Data` date NOT NULL,
  `NumeroPasti` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ristorante`
--

INSERT INTO `ristorante` (`IDPrenotazione`, `IDBagno`, `IDCliente`, `Data`, `NumeroPasti`) VALUES
(1, 1, 1, '2017-07-03', 3),
(2, 1, 2, '2017-07-26', 2),
(3, 2, 4, '2017-07-19', 4),
(4, 3, 2, '2017-07-15', 5),
(5, 3, 4, '2017-07-04', 2),
(6, 3, 12, '2017-06-29', 6),
(13, 1, 12, '2017-07-07', 2),
(14, 1, 18, '2017-08-10', 3),
(15, 1, 5, '2017-09-08', 3),
(16, 1, 13, '2017-07-14', 2),
(17, 1, 12, '2017-07-14', 4),
(18, 2, 16, '2017-07-17', 2),
(19, 2, 18, '2017-07-29', 3),
(20, 2, 9, '2017-06-06', 1),
(21, 3, 9, '2017-05-16', 3),
(22, 3, 11, '2017-05-18', 3),
(23, 3, 19, '2017-06-15', 1),
(26, 3, 11, '2017-07-28', 6);

-- --------------------------------------------------------

--
-- Struttura della tabella `soggiorni`
--

CREATE TABLE `soggiorni` (
  `IDSoggiorno` int(11) NOT NULL,
  `IDBagno` int(11) NOT NULL,
  `IDCliente` int(11) NOT NULL,
  `DataInizio` date NOT NULL,
  `DataFine` date NOT NULL,
  `IDOmbrellone` int(11) NOT NULL,
  `IDCabina` int(11) NOT NULL,
  `IDPacchetto` int(11) NOT NULL,
  `Saldofinale` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `soggiorni`
--

INSERT INTO `soggiorni` (`IDSoggiorno`, `IDBagno`, `IDCliente`, `DataInizio`, `DataFine`, `IDOmbrellone`, `IDCabina`, `IDPacchetto`, `Saldofinale`) VALUES
(37, 1, 26, '2017-06-14', '2017-07-04', 7, 0, 0, 282),
(39, 1, 23, '2017-06-01', '2017-07-01', 8, 0, 0, 33),
(40, 1, 7, '2017-06-02', '2017-07-13', 4, 6, 1, 249.3),
(42, 1, 6, '2017-08-30', '2017-09-05', 2, 0, 0, 68),
(43, 1, 18, '2017-10-18', '2017-09-21', 9, 0, 0, 315),
(44, 1, 17, '2017-07-07', '2017-07-26', 7, 0, 0, 360),
(45, 1, 26, '2017-08-12', '2017-07-15', 4, 0, 0, 585),
(46, 1, 12, '2017-07-04', '2017-07-05', 8, 10, 1, 39.6),
(47, 2, 7, '2017-06-08', '2017-06-08', 4, 0, 0, 12),
(48, 2, 17, '2017-06-10', '2017-06-28', 2, 5, 1, 188.1),
(49, 2, 14, '2017-06-30', '2017-07-05', 6, 0, 0, 435),
(50, 2, 19, '2017-07-15', '2017-08-15', 8, 9, 2, 300),
(51, 2, 26, '2017-08-29', '2017-08-31', 7, 0, 0, 63),
(52, 3, 6, '2017-05-01', '2017-07-01', 5, 5, 3, 450),
(53, 3, 12, '2017-07-07', '2017-07-11', 6, 0, 0, 75),
(54, 3, 0, '2017-07-26', '2017-07-27', 7, 4, 1, 39.6),
(55, 3, 0, '2017-08-16', '2017-07-27', 3, 9, 1, 638.1),
(56, 3, 0, '2017-08-31', '2017-09-13', 2, 0, 0, 118);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `animatori`
--
ALTER TABLE `animatori`
  ADD PRIMARY KEY (`IDAnimatore`),
  ADD KEY `IDBagno` (`IDBagno`);

--
-- Indici per le tabelle `bagni`
--
ALTER TABLE `bagni`
  ADD PRIMARY KEY (`IDBagno`);

--
-- Indici per le tabelle `cabine`
--
ALTER TABLE `cabine`
  ADD PRIMARY KEY (`IDCabina`,`IDBagno`),
  ADD KEY `IDBagno` (`IDBagno`);

--
-- Indici per le tabelle `clienti`
--
ALTER TABLE `clienti`
  ADD PRIMARY KEY (`IDCliente`);

--
-- Indici per le tabelle `listinoprezzi`
--
ALTER TABLE `listinoprezzi`
  ADD PRIMARY KEY (`IDPrezzo`);

--
-- Indici per le tabelle `ombrelloni`
--
ALTER TABLE `ombrelloni`
  ADD PRIMARY KEY (`IDOmbrellone`,`IDBagno`),
  ADD KEY `IDBagno` (`IDBagno`);

--
-- Indici per le tabelle `pacchetti`
--
ALTER TABLE `pacchetti`
  ADD PRIMARY KEY (`IDPacchetto`);

--
-- Indici per le tabelle `ristorante`
--
ALTER TABLE `ristorante`
  ADD PRIMARY KEY (`IDPrenotazione`),
  ADD KEY `IDCliente` (`IDCliente`),
  ADD KEY `IDBagno` (`IDBagno`);

--
-- Indici per le tabelle `soggiorni`
--
ALTER TABLE `soggiorni`
  ADD PRIMARY KEY (`IDSoggiorno`),
  ADD KEY `IDCliente` (`IDCliente`),
  ADD KEY `IDOmbrellone` (`IDOmbrellone`),
  ADD KEY `IDCabina` (`IDCabina`),
  ADD KEY `IDPacchetto` (`IDPacchetto`),
  ADD KEY `IDBagno` (`IDBagno`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `animatori`
--
ALTER TABLE `animatori`
  MODIFY `IDAnimatore` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT per la tabella `clienti`
--
ALTER TABLE `clienti`
  MODIFY `IDCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT per la tabella `ristorante`
--
ALTER TABLE `ristorante`
  MODIFY `IDPrenotazione` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT per la tabella `soggiorni`
--
ALTER TABLE `soggiorni`
  MODIFY `IDSoggiorno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `ristorante`
--
ALTER TABLE `ristorante`
  ADD CONSTRAINT `ristorante_ibfk_1` FOREIGN KEY (`IDBagno`) REFERENCES `bagni` (`IDBagno`);

--
-- Limiti per la tabella `soggiorni`
--
ALTER TABLE `soggiorni`
  ADD CONSTRAINT `soggiorni_ibfk_1` FOREIGN KEY (`IDBagno`) REFERENCES `bagni` (`IDBagno`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
