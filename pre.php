<?php
function delta_tempo ($data_iniziale,$data_finale,$unita) {

$data1 = strtotime($data_iniziale);
$data2 = strtotime($data_finale);

switch($unita) {
case "m": $unita = 1/60; break; //MINUTI
case "h": $unita = 1; break;	//ORE
case "g": $unita = 24; break;	//GIORNI
case "a": $unita = 8760; break; //ANNI
}

$differenza = (($data2-$data1)/3600)/$unita;
return $differenza;
}





  session_start();
  $servername = "localhost";
  $db = "progetto";
  $username = "root";
  $password = "";
  $numero = $_SESSION['bagno'];

  try {
   $conn = new PDO("mysql:host=$servername;dbname=$db", $username,$password);
   //se qualcosa va storto, si cattura l’eccezione, altrimenti..
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   }
  catch(PDOException $e)
   {
   echo "Connection failed: " . $e->getMessage();
   }

   $idc=$_POST['cliente'];
   $di=$_POST['inizio'];
   $df = $_POST['fine'];
   $ombrellone=$_POST['ombrelloni'];
   $cabina=$_POST['cabine'];
   $pacchetto = $_POST['pacchetto'];

   
   $mesemez=0;
   $mesemez2=0;
   $mesemez3=0;
   $cabinazza=0;
   $cabinazza2=0;
   $cabinazza3=0;
   $cabinazza4=0;
   $cabinazza5=0;

   //estrazione data inizio soggiorno
   $a1 = substr($di,0,4);
   $m1 = substr($di,5,2);
   $g1 = substr($di,8,2);

   //estrazione data fine soggiorno
   $a2 = substr($df,0,4);
   $m2 = substr($df,5,2);
   $g2 = substr($df,8,2);



     //calcolo saldo del cliente
     //CASO IN CUI CI SIA IL PACCHETTO
     if($pacchetto == 2){
       $saldo = 300;
     }
     else if ($pacchetto == 3){
       $saldo = 450;
     }
     else if ($pacchetto == 4){
       $saldo = 600;
     }
     else if ($pacchetto != 2 || $pacchetto != 3 || $pacchetto != 4){
       //caso in cui non c'è il pacchetto e devo calcolare paga per giorno
       if($m1 == $m2){//MESE UGUALE
         if($m1 == 5 || $m1 == 6 || $m1 == 9){//sono in MAGGIO,GIUGNO E SETTEMBRE cioè FASCIA BASSA
           if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){//sono nella BASSA fila
             $calcolo = delta_tempo($di,$df,"g");
             $calcolo++;
             $saldo = $calcolo * 8;
           }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){//sono nella MEDIA fila
             $calcolo = delta_tempo($di,$df,"g");
             $calcolo++;
             $saldo = $calcolo * 12;
           }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){//sono nella ALTA fila
             $calcolo = delta_tempo($di,$df,"g");
             $calcolo++;
             $saldo = $calcolo * 15;
           }
           if($cabina != 0){
             $calcolo = delta_tempo($di,$df,"g");
             $calcolo++;
             $cabinazza = $calcolo * 3;
             $saldo = $saldo + $cabinazza;
           }
         }else if($m1 == 7){//LUGLIO FASCIA MEDIA
           if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){//sono nella BASSA fila
             $calcolo = delta_tempo($di,$df,"g");
             $calcolo++;
             $saldo = $calcolo * 11;
           }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){//sono nella MEDIA fila
             $calcolo = delta_tempo($di,$df,"g");
             $calcolo++;
             $saldo = $calcolo * 15;
           }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){//sono nella ALTA fila
             $calcolo = delta_tempo($di,$df,"g");
             $calcolo++;
             $saldo = $calcolo * 18;
           }
           if($cabina != 0){
             $calcolo = delta_tempo($di,$df,"g");
             $calcolo++;
             $cabinazza = $calcolo * 4;
             $saldo = $saldo + $cabinazza;
           }
         }else if($m1 == 8){//AGOSTO FASCIA ALTA
           if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){//sono nella BASSA fila
             $calcolo = delta_tempo($di,$df,"g");
             $calcolo++;
             $saldo = ($calcolo+1) * 14;
           }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){//sono nella MEDIA fila
             $calcolo = delta_tempo($di,$df,"g");
             $calcolo++;
             $saldo = $calcolo * 18;
           }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){//sono nella ALTA fila
             $calcolo = delta_tempo($di,$df,"g");
             $calcolo++;
             $saldo = $calcolo * 21;
           }
           if($cabina != 0){
             $calcolo = delta_tempo($di,$df,"g");
             $calcolo++;
             $cabinazza = $calcolo * 5;
             $saldo = $saldo + $cabinazza;
           }
         }
       }
       else if($m1 == ($m2+1)){//DEVO CALCOLARE I GIORNI DEI MESI distanti un mese solo
         if($m1==5||$m1==6||$m1==9){
           if($m1==5){ //adesso prendo fuori i giorni mancanti del primo mese
             $g1 = 32 - $g1;
           }else if($m1==5){
             $g1 = 31 - $g1;
           }else if($m1==5){
             $g1 = 31 - $g1;
           } //ora moltiplico i giorni del mese 1 per il prezzo in base alla fascia
           if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
             $saldo = $g1 * 8;
           }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
              $saldo = $g1 * 12;
           }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
              $saldo = $g1 * 15;
           }
           if($cabina != 0){
             $cabinazza = $g1 * 3;
             $saldo = $saldo + $cabinazza;
           }
         }else if($m1==7){
            $g1 = 32 - $g1;
            if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
              $saldo = $g1 * 11;
            }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
               $saldo = $g1 * 15;
            }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
               $saldo = $g1 * 18;
            }
            if($cabina != 0){
              $cabinazza = $g1 * 4;
              $saldo = $saldo + $cabinazza;
            }
          }else if($m1==8){
            $g1 = 32 - $g1;
            if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
              $saldo = $g1 * 14;
            }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
               $saldo = $g1 * 18;
            }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
               $saldo = $g1 * 21;
            }
            if($cabina != 0){
              $cabinazza = $g1 * 5;
              $saldo = $saldo + $cabinazza;
            }
          }
          //ho appena calcolato i giorni rimanenti del primo mese ora devo fare i gg del secondo e aggiungerlo al saldo
          if($m2==5||$m2==6||$m2==9){
            if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
              $saldo2 = $g2 * 8;
            }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
               $saldo2 = $g2 * 12;
            }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
               $saldo2 = $g2 * 15;
            }
            if($cabina != 0){
              $cabinazza2 = $g2 * 3;
              $saldo = $saldo + $cabinazza2;
            }
          }else if($m2==7){
             if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
               $saldo2 = $g2 * 11;
             }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
                $saldo2 = $g2 * 15;
             }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
                $saldo2 = $g2 * 18;
             }
             if($cabina != 0){
               $cabinazza2 = $g2 * 4;
               $saldo = $saldo + $cabinazza2;
             }
           }else if($m2==8){
             if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
               $saldo2 = $g2 * 14;
             }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
                $saldo2 = $g2 * 18;
             }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
                $saldo2 = $g2 * 21;
             }
             if($cabina != 0){
               $cabinazza2 = $g2 * 5;
               $saldo = $saldo + $cabinazza2;
             }
           }
          $saldo = $saldo + $saldo2;
       }
       else if($m1 != $m2){

         if($m1==5||$m1==6||$m1==9){
           if($m1==5){ //adesso prendo fuori i giorni mancanti del primo mese
             $g1 = 32 - $g1;
           }else if($m1==5){
             $g1 = 31 - $g1;
           }else if($m1==5){
             $g1 = 31 - $g1;
           } //ora moltiplico i giorni del mese 1 per il prezzo in base alla fascia
           if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
             $saldo = $g1 * 8;
           }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
              $saldo = $g1 * 12;
           }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
              $saldo = $g1 * 15;
           }
           if($cabina != 0){
             $cabinazza = $g1 * 3;
             $saldo = $saldo + $cabinazza;
           }
         }else if($m1==7){
            $g1 = 32 - $g1;
            if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
              $saldo = $g1 * 11;
            }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
               $saldo = $g1 * 15;
            }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
               $saldo = $g1 * 18;
            }
            if($cabina != 0){
              $cabinazza = $g1 * 4;
              $saldo = $saldo + $cabinazza;
            }
          }else if($m1==8){
            $g1 = 32 - $g1;
            if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
              $saldo = $g1 * 14;
            }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
               $saldo = $g1 * 18;
            }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
               $saldo = $g1 * 21;
            }
            if($cabina != 0){
              $cabinazza = $g1 * 5;
              $saldo = $saldo + $cabinazza;
            }
          }


          echo "spesa mese di luglio ";
          echo $saldo;
          echo "<br>";
          //ho appena calcolato i giorni rimanenti del primo mese ora devo fare i gg del secondo e aggiungerlo al saldo
          if($m2==5||$m2==6||$m2==9){
            if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
              $saldo2 = $g2 * 8;
            }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
               $saldo2 = $g2 * 12;
            }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
               $saldo2 = $g2 * 15;
            }
            if($cabina != 0){
              $cabinazza2 = $g2 * 3;
              $saldo = $saldo + $cabinazza2;
            }
          }else if($m2==7){
             if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
               $saldo2 = $g2 * 11;
             }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
                $saldo2 = $g2 * 15;
             }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
                $saldo2 = $g2 * 18;
             }
             if($cabina != 0){
               $cabinazza2 = $g2 * 4;
               $saldo = $saldo + $cabinazza2;
             }
           }else if($m2==8){
             if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
               $saldo2 = $g2 * 14;
             }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
                $saldo2 = $g2 * 18;
             }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
                $saldo2 = $g2 * 21;
             }
             if($cabina != 0){
               $cabinazza2 = $g2 * 5;
               $saldo = $saldo + $cabinazza2;
             }
           }
           echo "spesa mese disettembre ";
           echo $saldo2;
           echo "<br>";

          $saldo = $saldo + $saldo2;

          if($m1 == 5 && $m2 == 7){//HO IN MEZZO GIUGNO
            if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
              $mesemez = 30 * 8;
            }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
               $mesemez = 30 * 12;
            }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
               $mesemez = 30 * 15;
            }
            $saldo = $saldo + $mesemez;
            if($cabina != 0){
              $cabinazza3 = 30 * 3;
              $saldo = $saldo + $cabinazza3;
            }
          }else if($m1 == 5 && $m2 == 8){//HO IN MEZZO GIUGNO e luglio
            if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
              $mesemez = 30 * 8;
              $mesemez2 = 31 * 11;
            }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
               $mesemez = 30 * 12;
               $mesemez2 = 31 * 15;
            }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
               $mesemez = 30 * 15;
               $mesemez2 = 31 * 18;
            }
            $saldo = $saldo + $mesemez + $mesemez2;
            if($cabina != 0){
              $cabinazza3 = 30 * 3;
              $cabinazza4 = 31 * 4;
              $saldo = $saldo + $cabinazza3 + $cabinazza4;
            }
          }else if($m1 == 5 && $m2 == 9){//HO IN MEZZO GIUGNO lug e ago
            if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
              $mesemez = 30 * 8;
              $mesemez2 = 31 * 12;
              $mesemez3 = 31 * 14;
            }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
               $mesemez = 30 * 12;
               $mesemez2 = 31 * 15;
               $mesemez3 = 31 * 18;
            }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
               $mesemez = 30 * 15;
               $mesemez2 = 31 * 18;
               $mesemez3 = 31 * 21;
            }
            $saldo = $saldo + $mesemez + $mesemez2 + $mesemez3;
            if($cabina != 0){
              $cabinazza3 = 30 * 3;
              $cabinazza4 = 31 * 4;
              $cabinazza5 = 31 * 5;
              $saldo = $saldo + $cabinazza3 + $cabinazza4 + $cabinazza5;
            }
          }else if($m1 == 6 && $m2 == 8){//HO IN MEZZO luglio
            if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
              $mesemez = 31 * 11;
            }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
               $mesemez = 31 * 15;
            }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
               $mesemez = 31 * 18;
            }
            $saldo = $saldo + $mesemez;
            if($cabina != 0){
              $cabinazza3 = 31 * 4;
              $saldo = $saldo + $cabinazza3;
            }
          }else if($m1 == 6 && $m2 == 9){//HO IN MEZZO lug e ago
            if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
              $mesemez = 31 * 11;
              $mesemez2 = 31 * 14;
            }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
               $mesemez = 31 * 15;
               $mesemez2 = 31 * 18;
            }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
               $mesemez = 31 * 18;
               $mesemez2 = 31 * 21;
            }
            $saldo = $saldo + $mesemez + $mesemez2;
            if($cabina != 0){
              $cabinazza3 = 31 * 4;
              $cabinazza4 = 31 * 5;
              $saldo = $saldo + $cabinazza3 + $cabinazza4;
            }
          }else if($m1 == 7 && $m2 == 9){//HO IN MEZZO ago
            if($ombrellone == 1 || $ombrellone == 2 || $ombrellone == 3){
              $mesemez = 31 * 14;
            }else if($ombrellone == 4 || $ombrellone == 5 || $ombrellone == 6){
               $mesemez = 31 * 18;
            }else if($ombrellone == 7 || $ombrellone == 8 || $ombrellone == 9){
               $mesemez = 31 * 21;
            }
            $saldo = $saldo + $mesemez;
            if($cabina != 0){
              $cabinazza3 = 31 * 5;
              $saldo = $saldo + $cabinazza3;
            }
          }
       }
       if($pacchetto == 1 && $ombrellone != 0 && $cabina != 0){
         $saldo *= 0.9;
       }
     }
   //inserting data order
   $toinsert = "INSERT INTO soggiorni
			(idbagno,idcliente, datainizio, datafine, IDOmbrellone, IDCabina, IDPacchetto, Saldofinale)
			VALUES
			( '$numero',
        '$idc',
        '$di',
        '$df',
        '$ombrellone',
			  '$cabina',
        '$pacchetto',
        '$saldo')";

      //declare in the order variable
      $result = $conn->exec($toinsert);	//order executes
      if($result){
	       echo("<br>Inserimento avvenuto correttamente");
      } else{
	        echo("<br>Inserimento non eseguito");
      }

      $sql = "UPDATE ombrelloni SET Occupato=1 WHERE IDOmbrellone=$ombrellone AND IDBagno=$numero";
      $query = $conn->query($sql);

      if($cabina == 0){
        $sql2 = "UPDATE cabine SET Occupata=0 WHERE IDCabina=$cabina AND IDBagno=$numero";
        $query = $conn->query($sql2);
      }
      else{
        $sql2 = "UPDATE cabine SET Occupata=1 WHERE IDCabina=$cabina AND IDBagno=$numero";
        $query = $conn->query($sql2);
      }


      header("location: http://localhost/BagnoJumper/bagno.php?");


 ?>
