<!DOCTYPE html>
<html lang="it">
<?php
  session_start();
  $servername = "localhost";
  $db = "progetto";
  $username = "root";
  $password = "";
  $nomee = $_SESSION['nome'];
  $numero = $_SESSION['bagno'];
  try {
   $conn = new PDO("mysql:host=$servername;dbname=$db", $username,$password);
   //se qualcosa va storto, si cattura l’eccezione, altrimenti..
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   }
  catch(PDOException $e)
   {
   echo "Connection failed: " . $e->getMessage();
   }

   $sql="SELECT * FROM animatori WHERE IDBagno=$numero";
   $query = $conn->query($sql);
   $nani = $query->rowCount();

   $sql="SELECT * FROM animatori WHERE IDBagno=$numero ORDER BY IDAnimatore";
   foreach($conn->query($sql) as $row){
     $ani[]=$row['IDAnimatore'];
     $nome[]=$row['Nome'];
     $cogn[]=$row['Cognome'];
     $cf[]=$row['CF'];
     $dn[]=$row['datanascita'];
     $da[]=$row['dataassunzione'];
     $paga[]=$row['Paga'];
   }

 ?>
<style media="screen">
table  {
    border-collapse:collapse
}
td, th {
    border:1px solid #ddd;
    padding:8px;
}
.tab{
  width: 60%;
}
</style>
  <head>
    <meta charset="utf-8">
    <title>Bagno Jumper - Animatori</title> <!-- Pagina generale per le Cabine, sarà la stessa per tutti, ma con php a seconda del bagno di origine cambiano i dati dentro -->
  </head>
  <body>
    <div>
      <h1 style="color: red"><center>Animatori - <?php echo "Bagno $nomee - $nani" ?></center></h1>
      <center>
        <table class="tab">
          <tr><td style="font-weight: bolder">IDAnimatore</td><td style="font-weight: bolder">Nome</td><td style="font-weight: bolder">Cognome</td><td style="font-weight: bolder">Cod. Fiscale</td><td style="font-weight: bolder">Data di Nascita</td><td style="font-weight: bolder">Data di Assunzione</td><td style="font-weight: bolder">Paga Oraria</td><td style="font-weight: bolder">Elimina</td></tr> <!-- A seconda della disponibilità del database il bottone sarà abilitato o meno -->
          <?php for($i=0;$i<$nani;$i++){
            echo "<tr><td>" ;
            echo $ani[$i] ;
            echo "</td><td>";
            echo $nome[$i] ;
            echo "</td><td>";
            echo $cogn[$i] ;
            echo "</td><td>";
            echo $cf[$i] ;
            echo "</td><td>";
            echo $dn[$i] ;
            echo "</td><td>";
            echo $da[$i] ;
            echo "</td><td>";
            echo $paga[$i] ;
            echo "</td><td>";
            echo "<center><a href=\"eliminaAni.php?id=$ani[$i]\">Elimina</a></center></td></tr>";
            }
          ?>
        </table>
      </ceter>
    </div>
  </body>
  <div style="padding: 20px">
    <center> <button  type="button" name="home" onclick="location.href='bagno.php';">Torna ai Servizi</button></center>
  </div>
  <div >
    <center> <button  type="button" name="home" onclick="location.href='home.php';">Home</button></center>
  </div>
</html>
